# first time or on update only?!
yarn install --production=false

yarn run esbuild src/im-*.js --bundle --minify --outdir=public/js
yarn run esbuild src/app.js --bundle --minify --outdir=public/js
yarn run esbuild src/sw.js --bundle --minify --outdir=public

#yarn run esbuild src/*.css --bundle --minify --outdir=public/css --loader:.woff=file --loader:.woff2=file
#yarn run esbuild src/font-varela-round.css --bundle --minify --outdir=public/css --loader:.woff=file --loader:.woff2=file
yarn run esbuild src/font-inter-variable.css --bundle --minify --outdir=public/css --loader:.woff=file --loader:.woff2=file
yarn run esbuild src/app.css --bundle --minify --outdir=public/css --external:/assets/*
yarn run esbuild src/im-*.css --bundle --minify --outdir=public/css --external:/assets/*
