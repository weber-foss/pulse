const addResourcesToCache = async (resources) => {
  const cache = await caches.open("v1");
  await cache.addAll(resources);
};

//var apiEndpoint = 'https://im-dev.weber-bodman.de';

/*
self.addEventListener("install", (event) => {
  event.waitUntil(
    addResourcesToCache([
      "/",
      "/index.html",
      //apiEndpoint + "/orderbook/files/fetch/*",
      //"/style.css",
      //"/app.js",
      //"/image-list.js",
      //"/star-wars-logo.jpg",
      //"/gallery/bountyHunters.jpg",
      //"/gallery/myLittleVader.jpg",
      //"/gallery/snowTroopers.jpg",
    ])
  );
});
*/

/*
self.addEventListener("fetch", (event) => {

    if (event.request.method !== "GET") return;

    // see: https://css-tricks.com/add-a-service-worker-to-your-site/
    if (event.request.headers.get('Accept').includes('text/html')) {
        // Handle HTML files...
        return;
    }

    event.respondWith(
        (async () => {
          // Try to get the response from a cache.
          const cache = await caches.open("dynamic-v1");
          const cachedResponse = await cache.match(event.request);
    
          if (cachedResponse) {
            // If we found a match in the cache, return it, but also
            // update the entry in the cache in the background.
            event.waitUntil(cache.add(event.request));
            return cachedResponse;
          }

          //// https://felixgerschau.com/how-to-communicate-with-service-workers/
          //{
          //  type: 'MESSAGE_IDENTIFIER',
          //}

          response = await fetch(event.request);

          //if (!response.ok && response.status === 401) {
          //  //return Promise.reject(response);
          //  //console.log('error', response);
          //  // e.g. https://pwa.im-dev.weber-bodman.de/demo.html
          //  
          //  //console.log(event.request.url);
          //  //console.log('sw', response);
          //  
          //  // TODO: trigger page reload / app.js (!) / event?!
          //} else {
          //  //console.log('success', response); 
          //}
          
          //notify(response.json());

          return response;
        })()
    );
});
*/

// todo: context based routing?!
// ...e.g. by event.data.type...?!
self.addEventListener('message', (event) => {
    notify(event.data);
});

async function notify(data) {
    self.clients.matchAll({
      includeUncontrolled: true,
      type: 'window'
    }).then((clients) => {
        if (clients && clients.length) {
          for (let client of clients) {
            client.postMessage(data);
          }
        }
    });
}
