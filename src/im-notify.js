const template = document.createElement('template');
template.innerHTML = `
  <link rel="stylesheet" href="/css/im-notify.css">
  <style>
    :host {
        display: inline-block;
        position: relative;
        width: 100%;
        user-select: none;
        box-sizing: border-box;
    }
    .alerts {
        display: none;
    }
    :host([alerts]) .alerts {
        display: inline-block;
    }
    #info {
        width: 100%;
        height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
        border-radius: 0.75em;
        /*background: rgb(99, 159, 188);*/
        font-family: "Inter Variable", sans-serif;
        gap: 1em;
        color: var(--primary);
        background: #e7eef6f0;
    }
    .info {
        font-size: 2em;
    }
    .icon {
        --ggs: 1.2;
    }

    .badge {
        background: var(--background);
        color: white;
        border-radius: 50em;
        padding: 0 0.75em;
        font-size: 1.25em
    }
  </style>`;

export class ImNotify extends HTMLElement {

    constructor() {
        super();
        this.attachShadow({ mode: 'open' });
        //this.endpoint = this.getAttribute('endpoint');
        this.injectFont();
    }

    connectedCallback() {
        this.shadowRoot.appendChild(template.content.cloneNode(true));
        let tpl = document.createElement('template');
        tpl.innerHTML = `<div id="info"><span class="icon gg-check-r"></span><tickets class="info" id="tickets"></tickets><span class="icon gg-bell alerts"></span><alerts id="alerts" class="info badge alerts"></alerts></div>`;
        this.shadowRoot.appendChild(tpl.content.cloneNode(true));
        this.alerts = this.shadowRoot.getElementById('alerts');
        this.tickets = this.shadowRoot.getElementById('tickets');
    }

    // TODO: working?
    attributeChangedCallback(name, oldValue, newValue) {
        console.log('name', name);
        if(name == 'alerts') {
            this.alerts.innerHTML = newValue;
        } else if(name == 'tickets') {
            this.ticket.innerHTML = newValue;
        }
    }

    setInfo(info, num) {
        if(num > 0) {
            this.setAttribute(info, num);
        } else {
            this.removeAttribute(info);
        }
    }

    status(info) {
        this.setInfo('alerts', info.alerts);
        this.alerts.innerHTML = info.alerts;
        this.setInfo('tickets', info.tickets);
        this.tickets.innerHTML = info.tickets;
    }

    injectFont() {
        if(!document.getElementById('font-inter-variable')) {
           const font = document.createElement("link");
           font.href = "/css/font-inter-variable.css";
           font.rel = "stylesheet";
           font.id = "font-varela-round";
           document.head.appendChild(font);
        }
    }
}

customElements.define('im-notify', ImNotify);
