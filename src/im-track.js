const template = document.createElement('template');
template.innerHTML = `
  <link rel="stylesheet" href="/css/im-track.css">
  <style>
    :host {
        display: inline-block;
        position: relative;
        width: 100%;
        user-select: none;
        box-sizing: border-box;
    }
    #tracker {
        width: 100%;
        height: 100%;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        /*background-color: rgba(255, 255, 255, 0.5);*/
        border-radius: 0.75em;
        /*background: rgba(99, 188, 163, 0.45);*/
        background: var(--background);
        /*background: linear-gradient(0deg, rgba(99,188,163,1) 0%, rgba(255,255,255,0) 100%);*/
        font-family: "Inter Variable", sans-serif;
        padding: 0.5em;
        box-sizing: border-box;
    }
    :host([tracking="false"]) #tracker {
        background: #e7eef6f0;
    }
    :host([tracking="true"]) #clock {
        color: white;
    }
    :host([tracking="true"]) #balance, :host([tracking="true"]) #checkin {
        background: whitesmoke;
        border-radius: 0.25em;
        padding: 0.25em;
    }
    #button {
        cursor: pointer;
        border: medium;
        margin: 20px 0px 0px;
        padding: 0px 12px;
        border-radius: 0.5em;
        font-size: 22px;
        height: 55px;
        max-width: 200px;
        width: 100%;
        background-color: white;
        transition: color 0.5s ease-in-out 0s;
        align-items: center;
        justify-content: center;
        gap: 1em;
        display: none;
    }
    :host([ubiquitous]) #button {
        display: flex;
    }
    :host([tracking="false"]) #button {
        color: #6d6d6d;
        box-shadow: rgb(235, 235, 235) 0px 6px 0px;
    }
    :host([tracking="true"]) #button {
        /*color: rgb(2, 133, 92);*/
        color: var(--primary);
        transform: translateY(6px);
    }
    #clock {
        font-size: 5em;
        color: var(--primary);
    }
    #balance {
        color: gray;
        margin-bottom: 10px;
    }

    #checkin {
        color: gray;
        margin-top: 10px;
    }

    @media screen and ( max-height: 479px )
    {
        #clock {
            display: none;  
        }
        #button {
            margin: 0 0.5em;
        }
    }
  </style>`;

export class ImTrack extends HTMLElement {

    constructor() {
        super();
        this.attachShadow({ mode: 'open' });
        this.endpoint = this.getAttribute('endpoint');
        this.injectFont();
        this.audioConnected = new Audio("/assets/connected.mp3");
        this.audioDisconnected = new Audio("/assets/disconnected.mp3");
    }

    connectedCallback() {
        this.shadowRoot.appendChild(template.content.cloneNode(true));
        let tpl = document.createElement('template');
        tpl.innerHTML = `<div id="tracker">
            <div id="balance"></div>
            <clock id="clock"></clock>
            <div id="checkin"><span id="checkin-time"></span></div>
            <button id="button"><i class="icon gg-time"></i> <span id="label">Start</span></button>
        </div>`;
        this.shadowRoot.appendChild(tpl.content.cloneNode(true));
        this.clock = this.shadowRoot.getElementById('clock');
        this.button = this.shadowRoot.getElementById('button');
        this.balance = this.shadowRoot.getElementById('balance');
        this.checkinTime = this.shadowRoot.getElementById('checkin-time');
        this.startTime();
        this.button.addEventListener("click", this.track.bind(this));
        this.label = this.shadowRoot.getElementById('label');
    }

    track() {
        fetch(`${this.endpoint}/timetracking/index/log`, { credentials: 'include' }).then(resp => {
            if(resp.status == 200) {
                resp.json().then(info => {
                    let checkedIn = info.hasOwnProperty('checkin');
                    this.status(info);
                    if(checkedIn) {
                        this.audioDisconnected.pause()
                        this.audioConnected.play();
                    } else {
                        this.audioConnected.pause();
                        this.audioDisconnected.play();
                    }
                });
            }
        });
    }

    status(info) {
        if(info.ubiquitous) {
            if(info.account !== null) {
                this.setAttribute('tracking', info.checkin !== null);
                this.balance.innerHTML = info.account;
            } else {
                this.removeAttribute('tracking');
            }

            if(info.checkin !== null) {
                this.checkinTime.innerHTML = info.checkin;
            } else {
                this.checkinTime.innerHTML = '--:--';
            }

            this.setAttribute('ubiquitous', '');
            this.label.textContent = (info.checkin !== null ? 'Stop' : 'Start');
        } else {
            this.removeAttribute('ubiquitous');
        }
    }

    startTime() {
        const today = new Date();
        let h = today.getHours();
        let m = today.getMinutes();
        let s = today.getSeconds();
        h = this.checkTime(h);
        m = this.checkTime(m);
        /*
        s = this.checkTime(s);
        this.clock.innerHTML =  h + ":" + m + ":" + s;
        setTimeout(startTime, 1000);
        */
        this.clock.innerHTML =  h + ":" + m;
        setTimeout(this.startTime.bind(this), 60000 - s * 1000);
    }

    checkTime(i) {
        if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
        return i;
    }

    injectFont() {
        if(!document.getElementById('font-inter-variable')) {
           const font = document.createElement("link");
           font.href = "/css/font-inter-variable.css";
           font.rel = "stylesheet";
           font.id = "font-inter-variable";
           document.head.appendChild(font);
        }
    }
}

customElements.define('im-track', ImTrack);
