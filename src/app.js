/*

// Eventually needed for multiple endpoints (explicit bearer token)
// TODO: logout for app only(?)

import Keycloak from 'keycloak-js';

keycloakLogin = async () => {
    console.log('key cloaking...');
    try {
      //const keycloak = Keycloak("keycloak.json");
      const keycloak = new Keycloak({
            url: 'https://sso.weber-bodman.de/auth/',
            realm: 'im-dev',
            clientId: 'pulse'
      });
      keycloak
        .init({
          onLoad: "login-required",
        })
        .then((authenticated) => {
          if (authenticated) {
            //call api to either login or register and log in the user
            console.log("user info");
            //console.log(keycloak.userInfo);
            keycloak.loadUserInfo().then((info) => console.log(info) );
          }
        });
    } catch (error) {
        console.log("error");
        throw error;
    }
  };

keycloakLogin();
*/

const registerServiceWorker = async () => {
    if ("serviceWorker" in navigator) {
        try {
            const registration = await navigator.serviceWorker.register("/sw.js", {
                scope: "/",
            });
            if (registration.installing) {
                console.log("Service worker installing");
            } else if (registration.waiting) {
                console.log("Service worker installed");
            } else if (registration.active) {
                console.log("Service worker active");
            }
        } catch (error) {
            console.log(error);
            console.error(`Registration failed with ${error}`);
        }
    }
};

registerServiceWorker();

let app = document.getElementById("app");
let list = document.querySelector("im-list");
let header = document.querySelector("header");
let modal = document.querySelector("modal");
let user = document.getElementById("user");
//let toggleListButton = document.getElementById("toggle-list");
let footer = document.querySelector("footer");
let search = document.getElementById("search");
let numpad = document.getElementById("numpad");
let processingIndicator = document.getElementById("processing");
let collectArticlesButton = document.getElementById("collect-articles");
let collectArticlesPrivateButton = document.getElementById("collect-articles-private");
let clearListButton = document.getElementById("clear-list");
let counter = document.getElementById("counter");
let track = document.getElementById("track");
let home = document.getElementById("home");
let notify = document.getElementById("notify");
let cards = document.getElementById("cards");
let entities = document.getElementById("entities");

var buttonQr = document.getElementById("button-qr");
//var homeButton = document.getElementById("button-home");

var scanner = document.querySelector("im-scanner");

var userInfo;

// REQUEST / RESPONSE interceptor...

const { fetch: originalFetch } = window;
window.fetch = async (...args) => {
    let [resource, options] = args;
    /*
    // e.g. Bearer...?!
    if (resource.includes('rapidapi')) {
        options.headers['X-RapidAPI-Key'] = 'your-rapid-key';
    }
    */

    const response = await originalFetch(resource, options);

    //console.log('res', response);

    // if (!response.ok && (response.status === 401 || response.status === 403 || response.status === 302)) {
    // || response.status === 403
    //if (response.status === 401) {
    if(response.status != 200) {        
        // force get bypassing cache (FF only)
        //location.reload(true);
        
        // target_link_uri= ?
        //location.href = 'https://im-dev.weber-bodman.de/otter?iss=https://pwa.im-dev.weber-bodman.de&redirect_target_uri=https://pwa.im-dev.weber-bodman.de';
        
        // response_mode=fragment ?
        //location.href = 'https://sso.weber-bodman.de/auth/realms/im-dev/protocol/openid-connect/auth?response_mode=fragment&client_id=im-dev&response_type=code&login=true&redirect_uri=https%3A%2F%2Fpwa.im-dev.weber-bodman.de';

        location.href = app.getAttribute('endpoint') + '/auth/login';

        //location.href = '/auth/redir?login=true&redirect=/';
        //location.href = '/auth';
        
        return Promise.reject(response);
    }

    return response;
};

// Routing
/*
function locationHashChanged() {
    let id = location.hash.substring(1);
    if (!isNaN(id)) {
        document.
    }
}

window.onhashchange = locationHashChanged;
*/

/*
function toggle() {
    if (list.hasAttribute('expanded')) {
        toggleOff();
    } else {
        toggleOn();
    }
}
*/

function toggleOn() {
    if (buttonQr.hasAttribute('active')) {
        scanNow();
    }
    footer.style.display = 'flex';
}

function toggleOff() {
    scanStop();
    footer.style.display = 'none';
    showList();
    //numpad.deactivate();
}

function stopScanner() {
    scanner.stopScanner();
    //header.style.display = 'none';
}

function startScanner() {
    scanner.startScanner();
    //header.style.display = 'flex';
    //homeButton.removeAttribute('active');
}

/*
search.addEventListener("keypress", (e) => {
    if (e.key === "Enter") {
        searchNum();
    }
});

search.addEventListener("focus", () => {
    //stopScanner();
    //window.scrollTo(0, 0);
});

search.addEventListener("blur", () => {
    if(!list.hasAttribute('expanded')) {
        startScanner();
    }
});
*/

search.addEventListener("click", (e) => {
    e.stopPropagation();
    if(!numpad.hasAttribute('active')) {
        search.setAttribute('active', '');
        numpad.activate();

        //stopScanner();
        //showList();
    } else {
        numpad.deactivate();
        search.removeAttribute('active');
    }
});

search.addEventListener('select', function() {
    this.selectionStart = this.selectionEnd;
}, false);

/*
toggleListButton.addEventListener("click", (e) => {
    e.stopPropagation();
    toggleList();
});
*/

function toggleList() {
    //if(list.style.display === 'block') {
    if(list.hasAttribute('active')) {
        hideList();
    } else {
        showList();
    }
}

function showList() {
    //track.removeAttribute('active');
    //home.removeAttribute('active');

    /*
    if(track.hasAttribute('tracking')) {
        homeButton.setAttribute('active', '');
    } else {
        homeButton.removeAttribute('active');
    }
    */
    //list.style.display = 'block';
    list.setAttribute('active', '');
    app.setAttribute('list-active', '');
    //toggleListButton.setAttribute('active', '');
}

function hideList(check) {
    if(check && !list.isEmpty()) {
        return;
    }
    //list.style.display = 'none';
    list.removeAttribute('active');
    app.removeAttribute('list-active');
    //toggleListButton.removeAttribute('active');
}

function hideLists(check) {
    hideList(check);
}

/*
function hideTrack() {
    track.removeAttribute('active');
}
*/

function showLists() {
    /*
    if(track.hasAttribute('tracking')) {
        track.setAttribute('active', '');
    }
    */
    home.setAttribute('active', '');
}

function hideLists(check) {
    hideList(check);
}

/*
function selectCard(id) {
    cards.childNodes.forEach(function (card, currentIndex, listObj) {
        //console.log(`${currentValue}, ${currentIndex}, ${this}`);
        if(card.id == id) {
            card.setAttribute('active', '');
        } else {
            card.removeAttribute('active');
        }
    // }, "myThisArg");
    });
}
*/

function searchNum() {
    let num = search.value;
    if (num && (!isNaN(num) || !isNaN(num.slice(1)))) {
        sendNum(num);
    }
}

function sendNum(num) {
    if(num.charAt(0) == 'A' && 'serviceWorker' in navigator) {
        navigator.serviceWorker.ready.then((registration) => {
            registration.active.postMessage({ type: 'scan', result: num });
        });
    } else {
        /*
        this.dispatchEvent(new CustomEvent("scan", {
            bubbles: true,
            cancelable: false,
            composed: true,
            detail: {
                result: num
            }
        }));
        */
        list.addItem(num);
    }
}

document.addEventListener("expanded", () => { header.style.display = 'none';toggleOff(); });

document.addEventListener("folded", () => { header.style.display = 'flex'; toggleOn(); });

//window.addEventListener("load", toggle);

window.addEventListener("numpad", (e) => {
    let k = e.detail.key;
    if (k === 'A') {
        if (search.value[0] === 'A') {
            search.value = search.value.slice(1);
        } else {
            search.value = 'A' + search.value;
        }
    //} else if (k === '×') {
    } else if (k === 'scan') {
        scanNow();
    //} else if (k === '↩') {
    } else if (k === 'search') {
        searchNum();
    //} else if (k === '↶') {
    } else if (k === 'backspace') {
        search.value = search.value.slice(0, -1);
    } else {
        search.value += k;
    }

    const articleNumberMatch = /^(20[0-9][0-9])(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])[0-9]{3}[1-9]$/;
    const articleNumberTrack = /^(2(0(\d(\d([0-1](((?<=0)[1-9]|(?<=1)[0-2])([0-3](((?<=0)[1-9]|(?<=[1-2])\d|(?<=3)[0-1])(\d{0,3}[1-9]?)?)?)?)?)?)?)?)?)$/;
    if(search.value.match(articleNumberMatch)) {
        search.removeAttribute('tracked');
        search.setAttribute('matched', '');
    } else if(search.value.match(articleNumberTrack)) {
        search.removeAttribute('matched');
        search.setAttribute('tracked', '');
    } else {
        search.removeAttribute('matched');
        search.removeAttribute('tracked');
    }
});

document.addEventListener("DOMContentLoaded", () => {
    location.hash = '#';
    checkUser();
});

document.addEventListener("visibilitychange", () => {
    if (document.visibilityState === "visible") {
        checkUser();
    }
});

checkUser = () => {
    user.onclick = function() { location.reload(true); };
    // redirect: 'manual'
    fetch(`${app.getAttribute('endpoint')}/authorization/users/info`, { credentials: 'include' }).then(resp => {
        // TODO: enhance correct nickname validation
        if(resp.status == 200) {
            resp.json().then(info => {
                
                user.textContent = info.nick[0];
                //user.onclick = function() { location.href = '/auth/redir?logout=https%3A%2F%2F' + encodeURIComponent(location.hostname); };
                // TODO: logout for app only(?)
                //user.onclick = function() { location.href = app.getAttribute('endpoint') + '/auth/redir?logout=https%3A%2F%2F' + encodeURIComponent(location.hostname); };
                user.onclick = function() { location.href = app.getAttribute('endpoint') + '/auth/logout'; };

                track.status(info);
                notify.status(info);

                if(!info.privacy && info.image) {
                    user.style.backgroundImage = `url('${app.getAttribute('endpoint')}/authorization/users/image')`;
                    user.innerHTML = '';
                } else if(info.color) {
                    user.style.backgroundColor = '#' + info.color;
                }

                // initial scanning
                /*
                if(userInfo == undefined && !track.hasAttribute('tracking')) {
                    scanNow();
                }
                */

                userInfo = info;
            });
        } else {
            modal.classList.add('max');
            user.textContent = 'Refresh';
        }
    });
};

function scanNow() {
    search.value = '';
    entities.setAttribute('scanning', '');
    startScanner();
}

function scanStop() {
    stopScanner();
    entities.removeAttribute('scanning');
}

buttonQr.addEventListener("click", (e) => {
    e.stopPropagation();
    if(buttonQr.hasAttribute('active')) {
        buttonQr.removeAttribute('active');
        scanStop();
    } else {
        buttonQr.setAttribute('active', '');
        scanNow();
    }
});

/*
homeButton.addEventListener("click", (e) => {
    e.stopPropagation();
    search.value = '';
    hideList();
    numpad.deactivate();
    stopScanner();
    track.setAttribute('active', '');
    homeButton.removeAttribute('active');
});
*/

// collect articles from list

collectArticlesButton.addEventListener("click", (e) => {
    e.stopPropagation();
    processingIndicator.setAttribute('active', '');
    list.collectArticles();
});

collectArticlesPrivateButton.addEventListener("click", (e) => {
    e.stopPropagation();
    processingIndicator.setAttribute('active', '');
    list.collectArticles(true);
});

// remove articles from list

clearListButton.addEventListener("click", (e) => {
    e.stopPropagation();
    list.clear();
});

document.addEventListener("numArticles", (e) => {
    if(e.detail.num > 0) {
        collectArticlesButton.setAttribute('active', '');
        collectArticlesPrivateButton.setAttribute('active', '');
        //processingIndicator.setAttribute('active', '');
    } else {
        collectArticlesButton.removeAttribute('active');
        collectArticlesPrivateButton.removeAttribute('active');
        //processingIndicator.removeAttribute('active');
        /*
        if(!numpad.hasAttribute('active')) {
            hideList();
        }
        */
    }

    numArticles(e);
});

document.addEventListener("syncedArticles", (e) => {
    processingIndicator.removeAttribute('active');
    //numArticles(e);
});

function numArticles(e) {
    counter.innerHTML = e.detail.num;

    if(list.isEmpty()) {
        clearListButton.removeAttribute('active');
        //hideList();
    } else {
        clearListButton.setAttribute('active', '');
        //showList();
    }

    if(e.detail.num > 0 || e.detail.show) {
        showList();
    } else {
        hideList();
    }
}

// height issues

/*
const documentHeight = () => {
    const doc = document.documentElement
    doc.style.setProperty('--doc-height', `${window.innerHeight}px`)
}
window.addEventListener('resize', documentHeight);
documentHeight();
*/

// mock data
/*
navigator.serviceWorker.ready.then((registration) => {
    //registration.active.postMessage({ type: 'scan', result: 'A333' });
    registration.active.postMessage({ type: 'scan', result: '2212' });
});
*/

/*
INSTALL promotion (not supported)
https://web.dev/learn/pwa/installation-prompt?hl=de
*/

// This variable will save the event for later use.
let deferredPrompt;
window.addEventListener('beforeinstallprompt', (e) => {
    // Prevents the default mini-infobar or install dialog from appearing on mobile
    e.preventDefault();
    // Save the event because you'll need to trigger it later.
    deferredPrompt = e;
    // Show your customized install prompt for your PWA
    // Your own UI doesn't have to be a single element, you
    // can have buttons in different locations, or wait to prompt
    // as part of a critical journey.
    showInAppInstallPromotion();
});

function showInAppInstallPromotion() {
    console.log('prompted');
}
