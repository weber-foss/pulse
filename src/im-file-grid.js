const template = document.createElement('template');
template.innerHTML = `
  <link rel="stylesheet" href="/css/im-file-grid.css">
  <style>
    :host {
        padding: 0;
        margin: 0;
        display: block;
        flex-grow: 1;
        overflow-y: auto;
        position: relative;
    }
    #grid {
        /*
        column-count: auto;
        column-width: 8rem;
        */

        /*
        display: flex;
        flex-wrap: wrap;
        justify-content: space-between;
        align-items: center;
        */

        list-style: none;
        padding: 0;
        margin: 0;
        overflow-x: hidden;
        overflow-y: auto;

        display: grid;
        grid-template-columns: repeat(auto-fit, minmax(160px, 1fr));
        align-items: center;
    }
    /*
    #grid::after {
        content: "";
        flex-grow: 1;
    }
    */
    #grid li {
        padding: 1em;
        text-align: center;
    }
    /*
    #empty {
        position: absolute;
        top: 50%;
        transform: translate(-50%, -50%) scale(3);
        left: 50%;
        font-size: 3em;
        color: gray;
    }
    */
    #files {
        display: none;
    }
    #add {
        position: fixed;
        bottom: 15px;
        right: 15px;
        background: #dfe7eedd;
        color: var(--primary);
        border-radius: 50em;
        z-index: 1;
        box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.1);
        border: 3px solid var(--primary);
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
        width: 4em;
        height: 4em;
    }
    :host([tags]) #add {
        visibility: hidden;
    }
    .file-upload .icon {
        --ggs: 3;
        cursor: pointer;
    }
    #grid progress {
        width: 100%;
        appearance: none;
        border-radius: 2px;
        border: none;
    }
    #grid progress[value] {
        color: #7e7e7e;
        background: #7e7e7e;
    }
    #grid .err {
        font-size: 3em;
        --ggs: 3;
        margin: 0.5em 0;
        color: #7e7e7e;
    }
    #grid .error {
        width: 100%;
        min-width: 80px;
        max-width: 160px;
        display: flex;
        justify-content: center;
    }
  </style>
  <div id="add">
    <form action="" method="post" enctype="multipart/form-data">
        <label for="files" class="file-upload">
            <span class="icon gg-math-plus"></span>
        </label>
        <input name="files" type="file" id="files" multiple />
    </form>
  </div>
  <ul id="grid"></ul>`;

export class ImFileGrid extends HTMLElement {

    constructor() {
        super();
        this.attachShadow({ mode: 'open' });
        this.endpoint = this.getAttribute('endpoint');
        this.files = new Map();
        this.client = null;
    }

    addFile(file) {
        if(!this.files.has(file.id)) {
            this.files.set(file.id, file);
            this.grid.insertAdjacentHTML('beforeend', `<li><im-file endpoint="${this.endpoint}/files" data-file-type="${file.type}" data-file-name="${file.name}" data-color="${file.color}" id="f-${file.id}" data-file-id="${file.id}" data-item="${this.dataset.item}" data-item-id="${this.dataset.itemId}"></im-file></li>`);
        }
        /*
        if(this.files.size > 0) {
            this.shadowRoot.getElementById('empty').style.display = 'none';
        }
        */
    }

    connectedCallback() {
        this.shadowRoot.appendChild(template.content.cloneNode(true));
        this.grid = this.shadowRoot.getElementById('grid');

        if(this.dataset.filter != 'disabled') {
            this.shadowRoot.getElementById("files").setAttribute('accept', 'image/*,.pdf');
        } else {
            this.shadowRoot.getElementById("files").setAttribute('accept', 'image/*,.pdf,.stp,.dxf');
        }

        this.loadFiles();

        this.shadowRoot.getElementById('files').addEventListener('change', () => {
            Array.from(this.shadowRoot.getElementById("files").files).forEach( file => {
                this.uploadFile(file);
            });
        });

        var that = this;

        document.addEventListener("tagUp", (e) => {
            if(e.detail.item == that.dataset.item && e.detail.itemId == that.dataset.itemId) {
                this.dataset.tagActive = e.detail.tagId;
                this.dataset.colorActive = e.detail.color;
                this.shadowRoot.getElementById('files').click();
            }
        });
    }

    send(data) {
        navigator.serviceWorker.ready.then((registration) => {
            registration.active.postMessage(data);
        });
    }

    loadFiles() {
        // Why needed? / connectedCallback()
        //if(!this.isLoaded) {
            fetch(`${this.endpoint}/files/list/${this.dataset.item}Id/${this.dataset.itemId}/`, {credentials: "include"}).then(resp => {
                resp.json().then(files => {
                    files.forEach((file) => {
                        this.addFile(file);
                    });
                });
            });
        //}
    }

    uploadFile(file)
    {
        const li = document.createElement("li");
        const prog = document.createElement("progress");

        prog.value = 0;

        li.appendChild(prog);

        // this.grid.insertBefore(li, this.shadowRoot.getElementById("add").nextSibling);
        this.grid.appendChild(li);
        
        var formData = new FormData();
        this.client = new XMLHttpRequest();
    
        prog.value = 0;
        prog.max = 100;
    
        // important parameter name
        formData.append("files", file);
    
        this.client.onerror = function(e) {
            console.log("onError");
        };

        var that = this;
    
        this.client.onload = function(e) {
            //prog.value = prog.max;
            prog.style.display = 'none';

            let f = JSON.parse(this.responseText);
            if(f[0].id) {
                li.insertAdjacentHTML('beforeend', `<im-file endpoint="${that.endpoint}/files" data-color="${that.dataset.colorActive}" id="f-${f[0].id}" data-item="${that.dataset.item}" data-item-id="${that.dataset.itemId}" data-file-id="${f[0].id}" data-file-name="${f[0].name}" data-file-type="${f[0].type}"></im-file>`);
                if(that.dataset.tagActive) {
                    fetch(`${that.endpoint}/files/tag/${that.dataset.item}Id/${that.dataset.itemId}/fileId/${f[0].id}/tagId/${that.dataset.tagActive}`, {credentials: "include"}).then(resp => {
                        resp.json().then(json => {
                            //console.log(json);
                            that.removeAttribute('data-tag-active'); 
                        });
                    });
                }
            } else {
                li.insertAdjacentHTML('beforeend', `<div class="error"><span class="gg-danger err"></span></div>`);
            }
        };
    
        this.client.upload.onprogress = function(e) {
            var p = Math.round(100 / e.total * e.loaded);
            prog.value = p;
        };
    
        this.client.open("POST", `${this.endpoint}/files/upload/${this.dataset.item}Id/${this.dataset.itemId}/`);
        this.client.withCredentials = true;
        this.client.send(formData);
    }
}

customElements.define('im-file-grid', ImFileGrid);
