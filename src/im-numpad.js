const template = document.createElement('template');
template.innerHTML = `<link rel="stylesheet" href="/css/im-numpad.css">`;

export class ImNumpad extends HTMLElement {

    constructor() {
        super();
        this.attachShadow({ mode: 'open' });
    }

    activate() {
        this.setAttribute('active', '');
    }
    
    deactivate() {
        this.removeAttribute('active');
    }

    connectedCallback() {
        this.shadowRoot.appendChild(template.content.cloneNode(true));
        let tpl = document.createElement('template');
        tpl.innerHTML = `<pad id="nums">
            <pad>
                <rw><a>1</a><a>2</a><a>3</a></rw>
                <rw><a>4</a><a>5</a><a>6</a></rw>
            </pad>
            <pad>
                <rw><a>7</a><a>8</a><a>9</a></rw>
                <rw><a disabled>-</a><a>0</a><a id="del" key="backspace"><i class="gg-backspace"></i></a></rw>
            </pad>
            <pad>
                <rw><a key="A"><i class="gg-tag"></i></a><a disabled key="C"><i class="gg-profile"></i></a><a disabled key="D"><i class="gg-file"></i></a></rw>
                <rw><!-- <a id="scan" key="scan"><i class="gg-qr"></i></a> --><a id="go" key="search"><i class="gg-search"></i></a></rw>
            </pad>
        </pad>`;
        
        this.shadowRoot.appendChild(tpl.content.cloneNode(true));

        this.keys = this.shadowRoot.querySelectorAll('#nums a');

        this.keys.forEach((key) => {
            key.addEventListener('click', (e) => {
                e.stopPropagation();
                if(!key.hasAttribute('disabled')) {
                    let keyText = key.hasAttribute('key') ? key.getAttribute('key') : key.text;
                    
                    this.dispatchEvent(new CustomEvent("numpad", {
                        bubbles: true,
                        cancelable: false,
                        composed: true,
                        detail: {
                            key: keyText
                        }
                    }));
                }
            });
        });
    }
}

customElements.define('im-numpad', ImNumpad);
