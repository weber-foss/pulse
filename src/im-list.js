// TODO: include "im-order.js" directly here?!

// See: https://codepen.io/Jeromche/pen/KKQrGgP
// See: https://neumorphism.io/#e0e0e0

import Hammer from 'hammerjs';

const template = document.createElement('template');
template.innerHTML = `
  <link rel="stylesheet" href="/css/im-list.css">
  <style>
    :host {
        padding: 0;
        margin: 0;
        display: block;
        position: relative;
    }
    #list {
        font-weight: bold;
        list-style: none;
        padding: 0;
        margin: 0;
        overflow-x: hidden;
    }
    #list li {
        padding: 0;
        position: relative;
    }
    #list li > .icon {
        position: absolute;
        color: #444444;
        font-size: 2em;
        top: 50%;
        left: 1em;
        --ggs: 2;
        transform: translate(-50%, -50%) scale(var(--ggs,1));
    }
    #list[expanded] li:not([expanded]) {
        display: none;
    }
    #empty {
        position: relative;
        overflow: hidden;
        height: 100%;
        text-align: center;
    }
    #empty .icon {
        font-size: 3em;
        --ggs: 3;
        color: #009865;
    }

    @keyframes rotate {
        0% {
            transform: rotate(0deg);
        }
        100% {
            transform: rotate(360deg);
        }
    }
    .gradient {
        display: inline-block;
        --size: 150px;
        --speed: 15s;
        --easing: cubic-bezier(0.8, 0.2, 0.2, 0.8);
        width: var(--size);
        height: var(--size);
        filter: blur(calc(var(--size) / 5));
        background-image: linear-gradient(rgba(55, 235, 169, 0.85), #5b37eb);
        animation: rotate var(--speed) var(--easing) alternate infinite;
        border-radius: 30% 70% 70% 30%/30% 30% 70% 70%;
    }

    .neo {
        border-radius: 21px;
        background: #edf2f8;
        box-shadow: inset 5px 5px 9px #cecece,
        inset -5px -5px 9px #f2f2f2;

        display: flex;
        align-items: center;
        justify-content: center;
        width: 100px;
        height: 100px;
        position: absolute;
        top: 50%;
        transform: translate(-50%, -50%) scale(var(--ggs,1));
        left: 50%;
    }

    #center {
        position: absolute;
        top: 50%;
        transform: translate(-50%, -50%) scale(var(--ggs,1));
        left: 50%;
    }
  </style>

  <div id="empty">
    <div id="center">
        <div class="gradient"></div>
        <div class="neo">
            <span class="icon gg-keyboard"></span>
        </div>
    </div>
  </div>
  <ul id="list"></ul>`;

export class ImList extends HTMLElement {

    constructor() {
        super();
        this.attachShadow({ mode: 'open' });
        this.endpoint = this.getAttribute('endpoint');
        this.items = new Map();
    }

    addItem(id) {
        var itemId;
        var itemType;

        var articleNumber;

        const articleNumberRegex = /2(0[0-9][0-9])(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])[0-9][0-9][0-9][0-9]/g;
        var articleNumberValidator = id.match(articleNumberRegex);
        if(articleNumberValidator) {
            articleNumber = id;
        }
        
        if(id[0] === 'A') {
            itemType = 'article';
            itemId = id.substr(1);
        } else if(articleNumber) {
            itemType = 'article';
            itemId = 'N' + id;
        } else {
            itemType = 'order';
            itemId = id;
        }

        var tId = itemType[0] + '-' + `${itemId}`;

        if(!this.items.has(tId)) {
            
            // not allowed without user interaction (e.g. when scanning right after starting PWA)
            // TODO: audio (e.g. from https://pixabay.com/de/sound-effects/search/beep/)
            // TODO: re-enable in case of (standard) dashboard for all users (ticket alerts etc.)
            //navigator.vibrate([500]);

            //this.send({ type: 'scan-hold' });
            this.items.set(tId, false);
            //this.orders.set(+id, order);
            this.list.insertAdjacentHTML('afterbegin', `<li data-item="${itemType[0]}-${itemId}">
                <span class="icon gg-play-list-check"></span>
                <im-${itemType} endpoint="${this.endpoint}" id="${tId}" data-item-id="${itemId}"></im-${itemType}>
            </li>`);
            this.addSwipes(tId);
            //this.send({ type: 'scan-response' });
        } else {
            // "scan once" scenario...
            //this.send({ type: 'scan-response' });
        }
        if(this.items.size > 0) {
            this.shadowRoot.getElementById('empty').style.display = 'none';
            /*
            if(!this.hasAttribute('active')) {
                location.hash = id;
            }
            */
        }
        if(itemType == 'order') {
            location.hash = id;
        }

        /*
        let numArticles = this.getArticleItems().length;
        this.dispatchEvent(new CustomEvent("numArticles", {
            bubbles: true,
            cancelable: false,
            composed: true,
            detail: {
                num: numArticles
            }
        }));
        */
    }

    removeItem(id) {
        this.#deleteItem(id);
        this.#numArticles(this.getArticleItems().length);
        this.#maintainVisibility();
    }

    removeItems() {
        var articleItems = this.getArticleItems();
        articleItems.forEach((elt) => {
            this.#deleteItem(elt[0]);
        });
        this.#numArticles(0);
        this.#maintainVisibility();
    }

    syncItems(items) {
        let articleItems = this.getArticleItems();
        articleItems.forEach((elt) => {
            let id = elt[0].substring(2);
            let el = this.shadowRoot.getElementById(elt[0]);
            if(items[id]?.hasPrivateCollector) {
                el.classList.add('hasPrivColl');
            } else {
                el.classList.remove('hasPrivColl');
            }
            if(items[id]?.hasCollector) {
                el.classList.add('hasColl');
            } else {
                el.classList.remove('hasColl');
            }
            //this.#deleteItem(elt[0]);
        });
        this.dispatchEvent(new CustomEvent("syncedArticles", {
            bubbles: true,
            cancelable: false,
            composed: true,
            /*
            detail: {
                num: n
            }
            */
        }));
        //this.#numArticles(0);
        this.#maintainVisibility();
    }

    #deleteItem(id) {
        let el = this.shadowRoot.getElementById(id);
        el.parentElement.remove();
        this.items.delete(id);
    }

    clearItems() {
        this.items.forEach((val, id) => {
            this.#deleteItem(id);
        });
        this.#numArticles(0);
        this.#hidden();
    }

    #maintainVisibility() {
        if(this.items.size < 1) {
            this.#hidden();
        }
    }

    #hidden() {
        this.shadowRoot.getElementById('empty').style.display = 'block';
    }

    getArticleItems() {
        return Array.from(this.items).filter((el) => { return el[1] && el[0][0] === 'a' } );
    }

    #numArticles(n) {
        this.dispatchEvent(new CustomEvent("numArticles", {
            bubbles: true,
            cancelable: false,
            composed: true,
            detail: {
                num: n
            }
        }));
    }

    foldItem(elem) {
        elem.style.left = "0px";
        elem.style.opacity = 1;
        elem.parentElement.style.background = 'none';

        window.location.hash = '#';
    }

    connectedCallback() {
        this.shadowRoot.appendChild(template.content.cloneNode(true));
        this.list = this.shadowRoot.getElementById('list');

        if ('serviceWorker' in navigator) {
            navigator.serviceWorker.addEventListener('message', event => {
                if(event.data.type === 'scan') {
                    this.addItem(event.data.result);
                } else if(event.data.type === 'rem-item') {
                    this.removeItem(event.data.id);
                } else if(event.data.type === 'rem-items') {
                    this.removeItems();
                } else if(event.data.type === 'sync-items') {
                    console.log(event.data);
                    this.syncItems(event.data.items);
                } else if(event.data.type === 'clear-items') {
                    this.clearItems();
                }
            });
        }

        /*
        this.shadowRoot.addEventListener("scan", function (event) {
            this.addItem(event.detail.result);
        });
        */

        var that = this;

        this.shadowRoot.addEventListener("articleLoaded", function (event) {
            if(event.detail.tId !== undefined) {
                that.items.set(event.detail.tId, true);
            }
            let numArticles = that.getArticleItems().length;
            this.dispatchEvent(new CustomEvent("numArticles", {
                bubbles: true,
                cancelable: false,
                composed: true,
                detail: {
                    num: numArticles,
                    show: true
                }
            }));
        });

        // Test
        //this.addItem(2209);
        //this.addItem(2212);
        /*
        for (let i = 0; i < 10; i++) { 
            this.addOrder(2209);
        }
        */

        this.shadowRoot.addEventListener("expanded", function (e) {
            Array.from(that.list.children).forEach((elt) => {
                if(e.target.id == elt.dataset.item) {
                    // set expanded to <li>...
                    elt.setAttribute('expanded', '');
                }
            });
            if(that.list.hasAttribute('expanded')) {
                e.stopPropagation();
            } else {
                that.list.setAttribute('expanded', '');
                that.setAttribute('expanded', '');
            }
        });

        this.shadowRoot.addEventListener("folded", function (e) {
            let hash = location.hash.substring(1);
            let navDisable = true;
            Array.from(that.list.children).forEach((elt) => {
                if(e.target.id == elt.dataset.item) {
                    elt.removeAttribute('expanded');
                    if(elt.dataset.item[0] == 'o') {
                        that.removeItem(elt.dataset.item);
                    }
                //} else if(elt.hasAttribute('expanded')) {
                } else if(elt.id == hash) {
                    navDisable = false;
                }
            });
            if(navDisable) {
                that.list.removeAttribute('expanded');
                that.removeAttribute('expanded');
                location.hash = '#';
            } else {
                e.stopPropagation();
            }
        });
    }

    send(data) {
        navigator.serviceWorker.ready.then((registration) => {
            registration.active.postMessage(data);
        });
    }

    addSwipes(id) {
        let el = this.shadowRoot.getElementById(id);
        var mc = new Hammer(el);

        if(id[0] !== 'o') {
            // domEvents: true
            // preventDefault: true
            mc.add( new Hammer.Pan({ direction: Hammer.DIRECTION_RIGHT, threshold: 60 }) );
            mc.on("pan", handleDrag);
        }

        var that = this;

        function handleDrag(e) {
            let elem = e.target;
            let posX = Math.max(0, e.deltaX);
            let max = elem.offsetWidth / 2;
            let threshold = 20;
    
            if(posX > 0) {
                if(posX < max) {
                    elem.style.left = posX + "px";
                    elem.style.opacity = (1 - posX / elem.offsetWidth * 2);
                    //elem.parentElement.style.background = 'rgba(220, 53, 69, ' + (posX / elem.offsetWidth + 0.5) + ')';
                    //rgb(220, 53, 69)
                    elem.parentElement.style.background = 'white';
                }
            }
            
            if (e.isFinal) {
                if(posX >= max - threshold) {
                    if(that.hasAttribute('expanded')) {
                        that.foldItem(elem);
                        // folded orders are removed automatically (no list entry; full-screen only)
                        if(elem.id[0] == 'a') {
                            that.send({ type: 'rem-item', id: elem.id });
                        }
                    } else {
                        that.send({ type: 'rem-item', id: elem.id });
                    }
                } else {
                    elem.style.left = "0px";
                    elem.style.opacity = 1;
                    elem.parentElement.style.background = 'none';
                }
                e.srcEvent.preventDefault();
            }
        }

        // TODO: fully enable articles, too (label printing, attachments etc.)...
        //if(id[0] === 'o') {
            //mc.add( new Hammer.Tap({ time: 2000, interval: 2000 }) );
            //mc.add( new Hammer.Tap() );
            mc.add( new Hammer.Tap({ time: 500 }) );
            //mc.get('tap').requireFailure('doubletap');
            mc.on("tap", (e) => {
                // id[0] === 'o' && 
                if(!e.target.hasAttribute('expanded') && !e.target.hasAttribute('error')) {
                    location.hash = e.target.id;
                } else {
                    //that.foldItem(e.target);
                }
            });
        //}
    }

    collectArticles(priv = false) {
        var articleIds = [];
        var articleItems = this.getArticleItems();
        articleItems.forEach((elt) => {
            articleIds.push(elt[0].substr(2));
        });

        //const headers = new Headers();
        //headers.append("Content-Type", "application/json");
        
        // Persist
        fetch(`${this.endpoint}/articles/articles/collect`, {
            credentials: "include",
            method: "POST",
            //headers: headers,
            body: JSON.stringify({
                private: priv,
                articleIds: articleIds
            })
        }).then(resp => {
            if(resp.ok) {
                //this.send({ type: 'rem-items' });
                resp.json().then((items) => {
                    this.send({ type: 'sync-items', items: items });
                });
            }
        });
    }

    clear() {
        this.send({ type: 'clear-items' });
    }

    isEmpty() {
        return this.items.size == 0;
    }
}

customElements.define('im-list', ImList);
