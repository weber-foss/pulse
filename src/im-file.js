const template = document.createElement('template');
template.innerHTML = `
  <style>
    :host {
        display: inline-block;
        position: relative;
        color: #444;
        width: 100%;
        /*font-weight: 100;*/
        font-weight: normal;
        line-height: 1.4;
        user-select: none;
        box-sizing: border-box;
    }
    :host([expanded]) {
        width: 100vw;
        height: 100vh;
        box-sizing: border-box;
        overflow-y: auto
    }
    #pending {
        position: absolute;
        top: 50%;
        transform: translate(-50%, -50%);
        left: 50%;
        font-size: 3em;
        color: gray;
    }
    img, ext {
        width: 100%;
        min-width: 80px;
        max-width: 160px;
        border-radius: 10px;
        border-width: 2px;
        border-style: solid;
        border-color: #ddd;
        padding: 2px;
        box-sizing: border-box;
        vertical-align: bottom;
    }
    ext {
        font-size: 2em;
        color: gray;
        text-decoration: none;
        font-weight: bold;
        padding: 0.5em;
        display: inline-block;
    }
    a {
        text-decoration: none;
    }
  </style>`;

export class ImFile extends HTMLElement {

    constructor() {
        super();
        this.attachShadow({ mode: 'open' });
        this.endpoint = this.getAttribute('endpoint');
    }

    connectedCallback() {
        this.shadowRoot.appendChild(template.content.cloneNode(true));
        let tpl = document.createElement('template');
        tpl.innerHTML = `<a target="_blank" href="${this.endpoint}/fetch/${this.dataset.item}Id/${this.dataset.itemId}/fileId/${this.dataset.fileId}/name/${this.dataset.fileName}">${this.getPreviewImageTpl()}</a>`;
        this.shadowRoot.appendChild(tpl.content.cloneNode(true));
    }

    getPreviewImageTpl() {
        var ext = this.dataset.fileName.split('.').slice(-1)[0];
        const previews = ['gif', 'jpg', 'jpeg', 'webp', 'pdf', 'png'];
        //if(this.dataset.fileType.startsWith('image') || this.dataset.fileType.startsWith('application/pdf')) {
        if(previews.includes(ext)) {
            return `<img style="border-color:${this.dataset.color}" loading="lazy" src="${this.endpoint}/fetch/${this.dataset.item}Id/${this.dataset.itemId}/size/medium/fileId/${this.dataset.fileId}">`;
        } else {
            return `<ext>.${ext}</ext>`;
        }
    }
}

customElements.define('im-file', ImFile);
