const template = document.createElement('template');
template.innerHTML = `
  <link rel="stylesheet" href="/css/im-article.css">
  <style>
    :host {
        display: flex;
        flex-direction: column;
        position: relative;
        color: #444;
        background: #e8eff6;
        /*font-weight: 100;*/
        font-weight: normal;
        line-height: 1.4;
        user-select: none;
    }
    :host(:active) .main {
        background: #f7f7f7;
    }
    :host([expanded]) {
        width: 100vw;
        height: 100dvh;
        box-sizing: border-box;
        overflow-y: hidden;
    }
    :host([expanded]) im-file-grid {
        display: block;
    }
    :host([expanded]) im-tag-grid[tags] {
        display: block;
    }
    .main {
        padding: 1em;
        min-height: 50px;
        display: flex;
        flex-direction: column;
    }

    :host(:not([expanded])) .main > .arrow {
        display: none;
    }
    :host(:not([expanded])) #btn-fold {
        display: none;
    }
    #btn-fold:hover {
        cursor: pointer;
    }
    #btn-fold {
        min-width: 1.5em;
        display: inline-block;
        vertical-align: top;
    }

    :host(:not([expanded])) .main {
        border-bottom: 1px dashed #4444443b;
    }
    :host([expanded]) .main {
        background: var(--background);
    }
    :host([expanded]) badge {
        display: none;
    }
    im-file-grid, im-tag-grid, #inventory {
        display: none;
    }
    :host([expanded]) #inventory {
        display: inline-flex;
    }
    #inventory {
        align-items: stretch;
        border-top: 2px solid var(--secondary);
        border-bottom: 2px solid var(--secondary);
        background: var(--secondary);
    }
    #inventory.changed {
        border-top: 2px solid var(--primary);
        border-bottom: 2px solid var(--primary);
        background: var(--primary);
    }
    #inventory-amount {
        padding: 0.5em;
        border: none;
        border-radius: 0.5em;
        font-size: 1.2em;
        flex-grow: 1;
        color: var(--secondary);
        min-width: 0;
    }
    #inventory.changed #inventory-amount {
        color: var(--primary);
    }
    #inventory-amount:focus-visible {
        outline: none;
    }
    #inventory-button {
        padding: 0 1em;
        color: white;
        display: inline-flex;
        align-items: center;
        cursor: pointer;
    }
    #inventory-unit {
        padding: 0.5em;
        display: inline-flex;
        align-items: center;
        color: white;
    }
    .label {
        line-height: 2;
    }
    .label.error {
        font-weight: bold;
        color: #dc3545;
        display: flex;
        justify-content: space-between;
        align-items: center;
    }
    cat {
        border-radius: 50em;
        padding: 0 0.6em;
        font-weight: 100;
        border: 1px solid gray;
        font-size: 0.75em;
        background: white;
    }
    type {
        padding: 0 1em;
        background: #444;
        color: #f1efed;
        margin-left: -0.833em;
        border-bottom-right-radius: 50em;
        border-top-right-radius: 50em;
        font-size: 1.2em;
    }
    #pending {
        position: absolute;
        top: 50%;
        transform: translate(-50%, -50%);
        left: 50%;
        font-size: 3em;
        color: gray;
    }

    /* Arrows */

    .arrow {
        border: solid #444;
        border-width: 0 3px 3px 0;
        display: inline-block;
        padding: 3px;
        width: 0.3em;
        height: 0.3em;
    }
    
    .arrow.right {
        transform: rotate(-45deg);
    }
    
    .arrow.left {
        transform: rotate(135deg);
    }
    
    .arrow.up {
        transform: rotate(-135deg);
    }
    
    .arrow.down {
        transform: rotate(45deg);
    }

    note {
        color: gray;
    }

    indicators {
        float: right;
        display: flex;
        align-items: center;
    }

    indicator {
        display: flex;
        gap: 0.5em;
        --ggs: 0.75;
        align-items: center;
        margin-left: 0.5em;
        font-weight: bold;
        font-size: 0.75em;
        color: #555;
        border-radius: 0.5em;
        min-height: 22px;
        min-width: 22px;
        display: flex;
        justify-content: center;
    }

    :host(.hasColl) indicator.coll, :host(.hasPrivColl) indicator.privColl {
        background: #be2fff;
        color: white;
    }

    header {
        display: flex;
        align-items: center;
    }

    thumb {
        flex-grow: 1;
        display: flex;
        flex-direction: column;
        align-content: right;
        align-items: end;
    }

    thumb .icon {
        display: inline-block;
        /*vertical-align: bottom;*/
    }

    thumb img, thumb .icon {
        max-height: 28px;
        color: #4444443b;
    }

    thumb img {
        box-shadow: 0 0 0 1px;
        /*border: 2px solid #444;*/
        border-radius: 2px;
        box-sizing: border-box;
        vertical-align: bottom;
    }

    number {
        padding-right: 0.6em;
        font-weight: 600;
    }

    badge {
        border-radius: 50em;
        background-color: var(--primary);
        padding: 0 0.6em 0 0.4em;
        gap: 0.5em;
        font-size: 0.8em;
        margin: 0 0.4em;
        line-height: 1.5;
        color: white;
        display: inline-flex;
        align-items: center;
        vertical-align: middle;
        --ggs: 0.5;
    }
  </style><span class="icon gg-loadbar-alt" id="pending"></span><div id="main" class="main"></div>`;

export class ImArticle extends HTMLElement {

    constructor() {
        super();
        this.attachShadow({ mode: 'open' });
        this.endpoint = this.getAttribute('endpoint');
        this.filesLoaded = false;
        this.tagsLoaded = false;
        this.article = null;
    }

    renderArticle() {
        let note = '';
        if(this.article.labelStandard) {
            note = `<note>(${this.article.labelStandard})</note>`;
        }

        let location = '';
        if(this.article.location) {
            location = `<indicator>${this.article.location}</indicator>`;
        }

        if(this.article.collId) {
            this.classList.add('hasColl');
            console.log('hasColl!');
        }

        if(this.article.collPrivId) {
            this.classList.add('hasPrivColl');
        }

        let coll = `<indicator class="coll"><i class="icon gg-shopping-cart"></i></indicator>`;

        let collPriv = `<indicator class="privColl"><i class="icon gg-bookmark"></i></indicator>`;

        let tpl = document.createElement('template');
        // image src: /api/articles/files/fetch-image/articleId/831/size/small
        // let imgPath = arr[i].numImages ? '/' + apiPrefix + '/articles/files/fetch-image/articleId/' + arr[i].id + '/size/small' : '';
        let img = this.article.numImages ? '<img src="' + this.endpoint + '/articles/images/fetch/articleId/' + this.article.id + '/size/medium">' : '<i class="icon gg-image"></i>';
        //tpl.innerHTML = `<span class="label"><cat style="border-color:${article.category.color}99;">${article.category.label}</cat><type>#A${article.id}</type></span><br><caption>${article.label}</caption>`;
        // #A${article.id}
        let att = this.article.numFiles ? `<badge><i class="gg-folder"></i>${this.article.numFiles}</badge>` : '';
        let stk = this.article.stock > 0 ? this.getInventoryBadgeTpl() : '';
        tpl.innerHTML = `<header>
            <a id="btn-fold"><i class="arrow left"></i></a>
            <number>${this.article.number}</number>
            <cat style="border-color:${this.article.color}99;">${this.article.category}</cat>
            <thumb>${img}</thumb>
        </header>
        <section>
            <div class="label">${this.article.label}${att}${stk}</div>
            ${note}<indicators>${location}${collPriv}${coll}</indicators>
        </section>`;
        return tpl.content.cloneNode(true);
    }

    renderInventory() {
        let stk = this.article.stock ? this.article.stock.toLocaleString() : '';
        let unt = this.article.unit ? this.article.unit : '-';
        let tpl = document.createElement('template');
        tpl.innerHTML = `<div id="inventory"><div id="inventory-unit">${unt}</div><input inputmode="decimal" id="inventory-amount" value="${stk}"><a id="inventory-button"><i class="gg-sync"></i></a></div>`;
        return tpl.content.cloneNode(true);
    }

    renderInventoryBadge() {
        let tpl = document.createElement('template');
        tpl.innerHTML = this.getInventoryBadgeTpl();
        return tpl.content.cloneNode(true);
    }

    getInventoryBadgeTpl() {
        let stk = this.article.stock ? this.article.stock.toLocaleString() : '';
        return `<badge id="badge-stock"><span id="badge-stock-amount">${stk}</span>${this.article.unit}</badge>`;
    }

    renderError(id) {
        let tpl = document.createElement('template');
        //tpl.innerHTML = `<span class="label"><span class="material-icons-outlined" style="float:right">error_outline</span>#A${id}</span><br>Error`;
        tpl.innerHTML = `<a id="btn-fold"><i class="arrow left"></i></a><span class="label error"><span>#A${id}</span><span class="icon gg-danger"></span></span>Error`;
        return tpl.content.cloneNode(true);
    }

    load(id) {
        var fetchUrl;
        if(id[0] === 'N') {
            fetchUrl = `${this.endpoint}/articles/articles/search/articleNumber/${id.substr(1)}/`;
        } else {
            fetchUrl = `${this.endpoint}/articles/articles/fetch/articleId/${id}/`;
        }
        fetch(fetchUrl, {credentials: "include"}).then(resp => {
            resp.json().then(article => {
                this.shadowRoot.getElementById('pending').style.display = 'none';

                let that = this;

                if(article.id) {
                    this.article = article;
                    this.dataset.itemId = article.id;
                    //this.shadowRoot.appendChild = `<span class="label"><cat style="border-color:${article.status.color}99;">${article.status.label}</cat>#${article.id}</span><br>${article.label}`;
                    this.main.append(this.renderArticle());
                    this.shadowRoot.append(this.renderInventory());
                    let inventory = this.shadowRoot.getElementById('inventory');
                    let inventoryButton = this.shadowRoot.getElementById('inventory-button');
                    let inventoryInput = this.shadowRoot.getElementById('inventory-amount');
                    inventoryButton.addEventListener('click', (e) => {
                        that.syncInventory(inventoryInput.value.toLocaleString('en-EN'));
                    });
                    this.shadowRoot.getElementById('inventory-amount').addEventListener('input', (e) => {
                        if(inventoryInput.value != article.stock) {
                            inventory.classList.add('changed');
                        } else {
                            inventory.classList.remove('changed');
                        }
                    });
                    this.notify(this.id);
                } else {
                    this.article = false;
                    //this.shadowRoot.append = `<span class="label"><span class="material-icons-outlined" style="float:right">error_outline</span>${id}</span><br>Error`;
                    this.main.append(this.renderError(id));
                    this.setAttribute('error', '');
                    this.notify(undefined);
                }

                this.shadowRoot.getElementById('btn-fold').addEventListener('click', (e) => {
                    that.fold();
                });
            });
        });
    }

    syncInventory(amount) {
        let inventory = this.shadowRoot.querySelector('#inventory');
        let badge = this.shadowRoot.querySelector('#badge-stock');
        if(inventory.classList.contains('changed')) {
            let fetchUrl = `${this.endpoint}/articles/articles/inventory/articleId/${this.dataset.itemId}?amount=${amount}`;
            fetch(fetchUrl, {credentials: "include"}).then(resp => {
                resp.json().then(answer => {
                    if(answer.amount == amount) {
                        this.article.stock = answer.amount;
                        inventory.classList.remove('changed');
                        if(badge && this.article.stock > 0) {
                            this.shadowRoot.querySelector('#badge-stock-amount').innerHTML = this.article.stock.toLocaleString();
                        } else if (this.article.stock > 0) {
                            this.shadowRoot.querySelector('.main .label').appendChild(this.renderInventoryBadge());
                        } else {
                            badge.remove();
                        }
                    }
                });
            });
        }
    }

    notify(id) {
        this.dispatchEvent(new CustomEvent("articleLoaded", {
            bubbles: true,
            cancelable: false,
            composed: true,
            detail: {
                tId: id
            }
        }));
    }

    connectedCallback() {
        this.shadowRoot.appendChild(template.content.cloneNode(true));

        this.main = this.shadowRoot.getElementById('main');

        this.load(this.dataset.itemId);

        var that = this;

        function locationHashChanged() {
            let id = location.hash.substring(1);
            if (id == that.id && !that.hasAttribute('expanded')) {
                that.expand();
            } else if(that.hasAttribute('expanded')) {
                that.fold();
            }
        }
        
        window.addEventListener("hashchange", locationHashChanged);

        //window.addEventListener("load", locationHashChanged);
        document.addEventListener("DOMContentLoaded", locationHashChanged);

        this.addEventListener("tags", () => {
            that.setAttribute('tags', '');
            that.shadowRoot.getElementById('grid').setAttribute('tags', '');
        });
    }

    expand() {
        this.setAttribute('expanded', '');
        this.dispatchEvent(new CustomEvent("expanded", {
            bubbles: true,
            cancelable: false,
            composed: true
        }));
        if(!this.filesLoaded) {
            let tplFiles = document.createElement('template');
            tplFiles.innerHTML = `<im-file-grid id="grid" endpoint="${this.endpoint}/articles" data-filter="disabled" data-item="article" data-item-id="${this.dataset.itemId}"></im-file-grid>`;
            //this.grid = this.shadowRoot.appendChild(tpl.content.cloneNode(true));
            this.shadowRoot.appendChild(tplFiles.content.cloneNode(true));
            this.filesLoaded = true;
        }
    }

    fold() {
        this.removeAttribute('expanded');
        this.dispatchEvent(new CustomEvent("folded", {
            bubbles: true,
            cancelable: false,
            composed: true
        }));
    }

    /*
    send(data) {
        navigator.serviceWorker.ready.then((registration) => {
            registration.active.postMessage(data);
        });
    }
    */
}

customElements.define('im-article', ImArticle);
