import Hammer from 'hammerjs';

const template = document.createElement('template');
template.innerHTML = `
  <link rel="stylesheet" href="/css/im-tag-grid.css">
  <style>
    :host {
        padding: 0;
        margin: 0;
        display: none;
        overflow-y: auto;
        position: relative;
        flex-shrink: 0;
        /*margin-bottom: env(safe-area-inset-bottom, 0);*/
        text-align: center;
    }
    :host([tags]) {
        display: block;
    }
    #grid {
        list-style: none;
        padding: 0.5em;
        margin: 0;
        padding-bottom: max(0.5em, calc(env(safe-area-inset-bottom, 0) + 0.5em));
        overflow-x: auto;
        overflow-y: hidden;
        grid-template-columns: 50% 50%;
        border-top: 2px solid #ddd;
        white-space: nowrap;
        scrollbar-width: none;
        position: relative;
    }
    #grid li {
        display: inline-block;
        margin: 0.5em;
    }
    #files {
        display: none;
    }
    im-tag {
        border-radius: 5px;
        border: 2px solid #0071e3;
        position: relative;
        display: flex;
    }
    label {
        cursor: pointer;
        padding: 5px;
        position: relative;
        display: flex;
        gap: 0.5em;
    }
    btn {
        color: white;
        display: flex;
        padding: 5px;
    }
    btn:hover {
        cursor: pointer;
    }
    /*
    im-tag:hover {
        opacity:0.75;
    }
    */
    /*
    im-tag:active {
        background: white;
        height: 100%;
        width: 100%;
        position: absolute;
        bottom: 0;
        left: 0;
        z-index: 100;
        display: flex;
        border-radius: 0;
        padding:0;
        margin: 0;
        border: none;
    }
    */
    .icon {
        display: inline-block;
    }
    im-tag.processed {
        color: white;
    }
    /* needed for loading bar to work */
    im-tag.pressing label {
        text-shadow: 1px 1px 0px white;
        opacity: 0.99;
    }
    im-tag ld {
        position: absolute;
        z-index: -1;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        transform: scaleX(0);
        transform-origin: 100% 50%;
        transition: transform 1s ease-out;
    }
    im-tag.pressing ld {
        transform: scaleX(1);
    }
    /*
    @supports (-webkit-touch-callout: none) {
        :host {
            -webkit-margin-after: max(env(safe-area-inset-bottom, 0.25em), 0.25em);
        }
    }
    */
  </style>
  <ul id="grid"></ul>`;

export class ImTagGrid extends HTMLElement {

    constructor() {
        super();
        this.attachShadow({ mode: 'open' });
        this.endpoint = this.getAttribute('endpoint');
        this.tags = new Map();
    }

    connectedCallback() {
        this.shadowRoot.appendChild(template.content.cloneNode(true));
        this.grid = this.shadowRoot.getElementById('grid');

        this.loadTags();
    }

    loadTags() {
        // Why needed? / connectedCallback()
        //if(!this.isLoaded) {
            fetch(`${this.endpoint}/tags/list/${this.dataset.item}Id/${this.dataset.itemId}/`, {credentials: "include"}).then(resp => {
                resp.json().then(tags => {
                    if(tags.length > 0) {
                        tags.forEach((tag) => {
                            this.addTag(tag);
                        });
                        this.setAttribute('tags', '');
                        this.dispatchEvent(new CustomEvent("tags", {
                            bubbles: true,
                            cancelable: false,
                            composed: true
                        }));
                    }
                });
            });
        //}
    }

    addTag(tag) {
        if(!this.tags.has(tag.id)) {
            this.tags.set(tag.id, tag);
            this.grid.insertAdjacentHTML('beforeend', `<li><im-tag data-color="${tag.color}" style="border-color:${tag.color};background-color:${tag.processed ? tag.color : 'none'};" class="${tag.processed ? 'processed' : ''}" id="t-${tag.id}" data-tag-id="${tag.id}" data-item="${this.dataset.item}" data-item-id="${this.dataset.itemId}"><label><ld style="background:${tag.color}"></ld><i class="icon gg-add"></i> ${tag.label}</label>${tag.processed ? '' : '<btn style="background:' + tag.color + '"><i class="icon gg-check"></i></btn>'}</im-tag></li>`);
            
            var elt = this.shadowRoot.getElementById('t-' + tag.id);

            var btn = this.shadowRoot.querySelector('#t-' + tag.id + ' btn');

            var label = this.shadowRoot.querySelector('#t-' + tag.id + ' label');

            var that = this;

            label.addEventListener('click', function(ev) {
                //elt.classList.remove('pressing');
                elt.dispatchEvent(new CustomEvent("tagUp", {
                    bubbles: true,
                    cancelable: false,
                    composed: true,
                    detail: {
                        item: that.dataset.item,
                        itemId: that.dataset.itemId,
                        tagId: tag.id,
                        color: tag.color
                    }
                }));
            });
            
            /*
            elt.addEventListener('click', () => {
                this.dispatchEvent(new CustomEvent("tagUp", {
                    bubbles: true,
                    cancelable: false,
                    composed: true
                }));
            });
            */

            if(!tag.processed) {

                var mc = new Hammer(btn);

                //mc.add( new Hammer.Press({ time: 750, event: 'press' }));
                mc.add( new Hammer.Press({ event: 'press' }));

                // needed to avoid "bugs" only, see:
                // https://github.com/hammerjs/hammer.js/issues/751
                mc.add( new Hammer.Pan({}));

                //mc.add( new Hammer.Press({ time: 1750, event: 'longPress' }));

                /*

                mc.add( new Hammer.Tap({ time: 500 }) );

                mc.on('tap', function(ev) {
                    console.log('tap: ' + that.dataset.orderId);
                    elt.classList.remove('pressing');
                    elt.dispatchEvent(new CustomEvent("tagUp", {
                        bubbles: true,
                        cancelable: false,
                        composed: true,
                        detail: {
                            orderId: that.dataset.orderId,
                            tagId: tag.id,
                            color: tag.color
                        }
                    }));
                });

                */

                mc.on('press', function(ev) {
                    //elt.style.backgroundColor = elt.dataset.color;
                    elt.classList.add('pressing');
                    //ev.srcEvent.stopPropagation();
                    ev.srcEvent.preventDefault();
                    ev.srcEvent.stopImmediatePropagation();
                /*}).on('longPress', function(ev) {
                    console.log(ev);
                    if(!elt.classList.contains('processed')) {
                        elt.classList.add('processed');
                        fetch(`${that.endpoint}/orderbook/tags/progress/orderId/${that.dataset.orderId}/hashId/${tag.id}`, {credentials: 'include', redirect: 'manual'}).then(resp => {
                            resp.json().then(j => {
                                if(j === true) {
                                    //elt.classList.add('processed');
                                    elt.style.backgroundColor = elt.dataset.color;
                                } else {
                                    elt.classList.remove('processed');
                                }
                            });
                        });
                    }*/
                }).on('pressup', function(ev) {
                    done(ev);
                }).on('panend', function(ev) {
                    elt.classList.remove('pressing');
                });

                btn.addEventListener("mouseleave", (event) => { elt.classList.remove('pressing'); });

                function done(ev) {
                    //console.log(ev);
                    //ev.srcEvent.stopPropagation();
                    //ev.srcEvent.preventDefault();

                    if(ev.deltaTime >= 1000 && !elt.classList.contains('processed')) {
                        elt.classList.add('processed');
                        fetch(`${that.endpoint}/tags/progress/${that.dataset.item}Id/${that.dataset.itemId}/hashId/${tag.id}`, {credentials: 'include', redirect: 'manual'}).then(resp => {
                            resp.json().then(j => {
                                if(j === true) {
                                    //elt.classList.add('processed');
                                    elt.style.backgroundColor = elt.dataset.color;
                                } else {
                                    elt.classList.remove('processed');
                                }
                                elt.classList.remove('pressing');
                            });
                        });
                    } else {
                        elt.classList.remove('pressing');
                    }
                }
            }
        }
    }
}

customElements.define('im-tag-grid', ImTagGrid);
