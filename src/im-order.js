const template = document.createElement('template');
template.innerHTML = `
  <link rel="stylesheet" href="/css/im-order.css">
  <style>
    :host {
        display: flex;
        flex-direction: column;
        position: relative;
        color: #444;
        background: #e8eff6;
        /*font-weight: 100;*/
        font-weight: normal;
        line-height: 1.4;
        user-select: none;
    }
    :host(:active) .main {
        background: #f7f7f7;
    }
    :host([expanded]) {
        width: 100vw;
        height: 100dvh;
        box-sizing: border-box;
        overflow-y: hidden;
    }
    :host([expanded]) im-file-grid {
        display: block;
    }
    :host([expanded]) im-tag-grid[tags] {
        display: block;
    }
    :host([error]) #grid {
        display: none;
    }
    .main {
        padding: 1em;
        min-height: 50px;
    }
    :host(:not([expanded])) .main > .arrow {
        display: none;
    }
    :host(:not([expanded])) #btn-fold {
        display: none;
    }
    #btn-fold:hover {
        cursor: pointer;
    }
    #btn-fold {
        min-width: 1.5em;
        display: inline-block;
    }
    :host(:not([expanded])) .main {
        border-bottom: 1px dashed #4444443b;
    }
    :host([expanded]) .main {
        background: #dfe7ee;
    }
    im-file-grid, im-tag-grid {
        display: none;
    }
    .label {
        font-weight: 600;
        font-size: 1.2em;
    }
    cat {
        float: right;
        border-radius: 1000px;
        font-size: 0.75em;
        padding: 0 0.6em;
        font-weight: 100;
        border: 3px solid gray;
    }
    #pending {
        position: absolute;
        top: 50%;
        transform: translate(-50%, -50%) scale(3);
        left: 50%;
        font-size: 3em;
        color: gray;
    }

    /* Arrows */

    .arrow {
        border: solid #444;
        border-width: 0 3px 3px 0;
        display: inline-block;
        padding: 3px;
        width: 0.3em;
        height: 0.3em;
    }
    
    .arrow.right {
        transform: rotate(-45deg);
    }
    
    .arrow.left {
        transform: rotate(135deg);
    }
    
    .arrow.up {
        transform: rotate(-135deg);
    }
    
    .arrow.down {
        transform: rotate(45deg);
    }
  </style><span class="icon gg-loadbar-alt" id="pending"></span><div id="main" class="main"></div>`;

export class ImOrder extends HTMLElement {

    constructor() {
        super();
        this.attachShadow({ mode: 'open' });
        this.endpoint = this.getAttribute('endpoint');
        this.filesLoaded = false;
        this.tagsLoaded = false;
    }

    renderOrder(order) {
        let tpl = document.createElement('template');
        let label = order.label ? order.label : ((order.articleSet && order.articleSet.article) ? order.articleSet.article.label : '');
        tpl.innerHTML = `<a id="btn-fold"><i class="arrow left"></i></a><span class="label"><cat style="border-color:${order.status.color}99;">${order.status.label}</cat>#${order.id}</span><br>${label}`;
        return tpl.content.cloneNode(true);
    }

    renderError(id) {
        let tpl = document.createElement('template');
        tpl.innerHTML = `<a id="btn-fold"><i class="arrow left"></i></a><span class="label"><span class="icon gg-danger" style="float:right"></span>${id}</span><br>Error`;
        return tpl.content.cloneNode(true);
    }

    load(id) {
        fetch(`${this.endpoint}/orderbook/orders/fetch/orderId/${id}/`, {credentials: "include"}).then(resp => {
            resp.json().then(order => {
                this.shadowRoot.getElementById('pending').style.display = 'none';
                if(order.id) {
                    this.order = order;
                    //this.shadowRoot.appendChild = `<span class="label"><cat style="border-color:${order.status.color}99;">${order.status.label}</cat>#${order.id}</span><br>${order.label}`;
                    this.main.append(this.renderOrder(order));
                } else {
                    //this.shadowRoot.append = `<span class="label"><span class="material-icons-outlined" style="float:right">error_outline</span>${id}</span><br>Error`;
                    this.setAttribute('error', '');
                    this.main.append(this.renderError(id));
                }
                let that = this;
                this.shadowRoot.getElementById('btn-fold').addEventListener('click', (e) => {
                    that.fold();
                    //that.remove();
                });
            });
        });
    }

    connectedCallback() {
        this.shadowRoot.appendChild(template.content.cloneNode(true));

        this.main = this.shadowRoot.getElementById('main');

        this.load(this.dataset.itemId);

        var that = this;

        function locationHashChanged() {
            let id = location.hash.substring(1);
            if (!isNaN(id) && id == that.dataset.itemId && !that.hasAttribute('expanded')) {
                that.expand();
            } else if(that.hasAttribute('expanded')) {
                that.fold();
                //that.remove();
            }
        }
        
        window.addEventListener("hashchange", locationHashChanged);

        //window.addEventListener("load", locationHashChanged);
        document.addEventListener("DOMContentLoaded", locationHashChanged);

        this.addEventListener("tags", () => {
            that.setAttribute('tags', '');
            that.shadowRoot.getElementById('grid').setAttribute('tags', '');
        });
    }

    expand() {
        this.setAttribute('expanded', '');
        this.dispatchEvent(new CustomEvent("expanded", {
            bubbles: true,
            cancelable: false,
            composed: true
        }));
        if(!this.filesLoaded) {
            let tplFiles = document.createElement('template');
            tplFiles.innerHTML = `<im-file-grid id="grid" endpoint="${this.endpoint}/orderbook" data-item="order" data-item-id="${this.dataset.itemId}"></im-file-grid>`;
            //this.grid = this.shadowRoot.appendChild(tpl.content.cloneNode(true));
            this.shadowRoot.appendChild(tplFiles.content.cloneNode(true));
            this.filesLoaded = true;
        }
        if(!this.tagsLoaded) {
            let tplTags = document.createElement('template');
            tplTags.innerHTML = `<im-tag-grid id="tags" endpoint="${this.endpoint}/orderbook" data-item="order" data-item-id="${this.dataset.itemId}"></im-tag-grid>`;
            this.shadowRoot.appendChild(tplTags.content.cloneNode(true));
            this.tagsLoaded = true;
        }
    }

    fold() {
        /*delete */this.removeAttribute('expanded');
        this.dispatchEvent(new CustomEvent("folded", {
            bubbles: true,
            cancelable: false,
            composed: true
        }));
    }

    /*
    remove() {
        this.send({ type: 'rem-item', id: this.id });
    }

    send(data) {
        navigator.serviceWorker.ready.then((registration) => {
            registration.active.postMessage(data);
        });
    }
    */
}

customElements.define('im-order', ImOrder);
