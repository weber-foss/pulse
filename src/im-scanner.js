import { BrowserQRCodeReader } from '@zxing/library';
//import { BrowserQRCodeReader } from '@zxing/browser';

const template = document.createElement('template');
template.innerHTML = `
  <link rel="stylesheet" href="/css/im-scanner.css">
  <style>
    :host {
        padding: 0;
        margin: 0;
        display: flex;
        position: relative;
        width: 100%;
        background: #0d0d22;
        overflow: hidden;
        justify-content: center;
        align-items: center;
    }
    #video {
        vertical-align: bottom;
        object-fit: cover;
        opacity: 0.33;
        min-width: 100%;
        min-height: 100%;
        height: auto;
        width: auto;
    }
    #target {
        max-width: 50%;
        max-height: 50%;
        position: absolute;
        top: 50%;
        left: 50%;
        margin-right: -50%;
        transform: translate(-50%, -50%);
        /*stroke: #009865dd;*/
        stroke: var(--primary);
        stroke-width: 8%;
        stroke-linejoin: round;
        /*
        stroke-linecap: round;
        */
    }
    menu {
        position: absolute;
        bottom: 0;
        right: 0;
        z-index: 10;
        user-select: none;
        margin: 0;
    }
    menu span {
        color: white;
        margin: 1em;
        cursor: pointer;
        -webkit-tap-highlight-color: transparent;
        -webkit-touch-callout: none;
        -webkit-user-select: none;
        -khtml-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    menu span:focus {
        outline: none !important;
    }
    #target g {
        transform-origin: 50% 50%;
    }
  </style>
  <menu>
    <span class="icon gg-bolt" id="flash" style="display:none"></span>
    <span class="icon gg-log-off" id="disabled"></span>
  </menu>
  <video id="video" width="100%" height="auto"></video>
  <svg id="target" viewBox="-2 -2 104 104">
    <path d="M25,2 L2,2 L2,25" fill="none" />
    <path d="M2,75 L2,98 L25,98" fill="none" />
    <path d="M75,98 L98,98 L98,75" fill="none" />
    <path d="M98,25 L98,2 L75,2" fill="none" />
  </svg>`;

  // eventually add <path >... into <g transform> block adding the following, too:
  // <animateTransform attributeType="XML" attributeName="transform" type="scale" values="1;1.04;1" additive="sum" begin="0s" dur="2s" repeatCount="indefinite"/>

export class ImScanner extends HTMLElement {
    constructor() {
        super();
        this.attachShadow({ mode: 'open' });
    }

    stopScanner() {
        //this.style.display = 'none';
        this.codeReader.reset();
        this.removeAttribute('active');
    }

    startScanner() {
      //this.style.display = 'block';
      //console.log('starting scanner...');
      //const constraints = { facingMode: 'environment', "video": { width: { exact: 100 } } };
      const constraints = { facingMode: 'environment' };
      const mediaStreamConstraints = { video: constraints };

      //codeReader.decodeFromInputVideoDeviceContinuously(selectedDeviceId, video, (result, err) => {
      // not decoding: codeReader.decodeOnceFromConstraints(mediaStreamConstraints, this.video, (result, err) => {
      
      //this.codeReader.decodeOnceFromConstraints(mediaStreamConstraints, this.video).then(handleResult);
      this.codeReader.decodeFromConstraints(mediaStreamConstraints, this.video, handleResult);

      /*
      //console.log('aha', this.codeReader.isMediaDevicesSuported);
      console.log('aha', this.codeReader.isVideoPlaying(this.video));

      if (!this.codeReader.isVideoPlaying(this.video)) {
        this.shadowRoot.getElementById('disabled').style.display = 'inline-block';
      }
      */

      this.setAttribute('active', '');

      var that = this;

      function handleResult(result) {
        if (result) {
            if ('serviceWorker' in navigator) {
                //codeReader.stopContinuousDecode();
                //this.stopScanner();
                that.send({type: 'scan', result: result.text});
            }
        }
        //if (err) { ...
      }
    }

    connectedCallback() {
      this.shadowRoot.appendChild(template.content.cloneNode(true));

      let video = this.video = this.shadowRoot.getElementById('video');

      let disabled = this.shadowRoot.getElementById('disabled');

      let flash = this.shadowRoot.getElementById('flash');

      this.video.addEventListener('playing', () => {
        disabled.style.display = 'none';
        if(this.hasFlash()) {
            flash.style.display = 'inline-block';
        }
        video.style.opacity = 1;
      });
      
      ['pause', 'error', 'stalled'].forEach(function(e) {
        video.addEventListener(e, stopped);
      });

      var that = this;

      disabled.addEventListener('click', () => {
        var constraints = { video: true };
        navigator.mediaDevices.getUserMedia(constraints).then(() => {
            that.stopScanner();
            that.startScanner();
        }).catch(function(err) {
            console.log("Error: " + err.message);
        });
      });

      flash.addEventListener('click', () => {
        if(flash.hasAttribute('flash')) {
            this.flash(false);
            flash.removeAttribute('flash');
        } else {
            this.flash(true);
            flash.setAttribute('flash', '');
        }
      });

      function stopped() {
        disabled.style.display = 'inline-block';
        flash.style.display = 'none';
        video.style.opacity = 0.33;
      }

      this.codeReader = new BrowserQRCodeReader();

      /*
      this.codeReader.getVideoInputDevices()
      //BrowserQRCodeReader.listVideoInputDevices()
        .then((videoInputDevices) => {
          this.startScanner();
        })
        .catch((err) => {
          console.error(err);
        })
      */

        //if ('serviceWorker' in navigator) {
        //    navigator.serviceWorker.addEventListener('message', event => {
        //        if(event.data.type === 'scan-response') {
        //            this.startScanner();
        //        } else if(event.data.type === 'scan-hold') {
        //            this.stopScanner();
        //        }
        //    })
        //}
    }

    send(data) {
        navigator.serviceWorker.ready.then((registration) => {
            registration.active.postMessage(data);
        });
    }

    hasFlash() {
        return this.codeReader && 
            this.codeReader.stream.getVideoTracks()[0] &&
            this.codeReader.stream.getVideoTracks()[0].getCapabilities instanceof Function &&
            this.codeReader.stream.getVideoTracks()[0].getCapabilities().torch;
    }

    flash(status) {

        //console.log(this.codeReader.stream.getVideoTracks()[0].getCapabilities());

        if(this.hasFlash()) {
            this.codeReader.stream.getVideoTracks()[0].applyConstraints({
                advanced: [{torch: status}]
            });
        }

        /*
        var imageCaptureConfig = {
          fillLightMode: "torch", // or "flash"
          focusMode: "continuous"
        };

        const track = this.codeReader.stream.getVideoTracks()[0];
        var imageCapture = new ImageCapture(track);

        imageCapture.setOptions(imageCaptureConfig);
        */
    }
}

customElements.define('im-scanner', ImScanner);