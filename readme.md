# Inmax PWA

Inmax Microservices.

## Build

yarn run esbuild src/im-*.js --bundle --sourcemap --outdir=public/js

yarn run esbuild src/*.css --bundle --sourcemap --outdir=public/css --external:/assets/*

(yarn run esbuild src/*.css --bundle --sourcemap --outdir=public/css --loader:.woff=file --loader:.woff2=file)

yarn run esbuild src/font-varela-round.css --bundle --sourcemap --outdir=public/css --loader:.woff=file --loader:.woff2=file

### production

(yarn install --production=false)

yarn run esbuild src/im-*.js --bundle --minify --outdir=public/js
yarn run esbuild src/app.js --bundle --minify --outdir=public/js
yarn run esbuild src/sw.js --bundle --minify --outdir=public
yarn run esbuild src/*.css --bundle --minify --outdir=public/css --external:/assets/*
yarn run esbuild src/font-inter-variable.css --bundle --minify --outdir=public/css --loader:.woff=file --loader:.woff2=file

### Dev

yarn run esbuild src/app.js --bundle --sourcemap --outdir=public/js
yarn run esbuild src/app.css --bundle --sourcemap --outdir=public/css --external:/assets/*

## TODO

- Scanner performance: https://github.com/maslick/koder
- load app resources from weber-bodman.de (central storage location)
